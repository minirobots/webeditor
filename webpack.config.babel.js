import path from 'path';
import { LoaderOptionsPlugin, DefinePlugin } from 'webpack';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import WriteFilePlugin from 'write-file-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import KoutoSwiss from 'kouto-swiss';

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, './app/index.js'),

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: './dist'
  },

  devServer: {
    publicPath: './dist',
    contentBase: './dist',
    historyApiFallback: true,
    host: '0.0.0.0',
    port: 9000,
    // progress: true  // invalid
    disableHostCheck: true
  },

  devtool: 'source-map',

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['lib', 'node_modules']
  },

  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: __dirname,
      verbose: true,
      dry: false
    }),
    new WriteFilePlugin(),
    new LoaderOptionsPlugin({
      options: {
        postcss: {},
        stylus: {
          use: [KoutoSwiss()]
        }
      }
    }),
    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, 'index.html')
    }, {
      from: path.resolve(__dirname, './blockly'),
      to: './blockly'
    }, {
      from: path.resolve(__dirname, 'indexEditor.html')
    }]),
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ],

  module: {
    rules: [
      {
        test: /\.xml$/,
        use: [{
          loader: 'raw-loader'
        }]
      },

      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: '10000',
            name: '[name].[ext]?[hash]',
            publicPath: 'fonts/',
            outputPath: 'fonts/'
          }
        }]
      }, {
        test: /\.js[x]?$/,
        use: [{
          loader: 'babel-loader',
          options: {
            plugins: [
              ['import', { libraryName: 'antd', style: 'css' }]
            ]
          }
        }],
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'postcss-loader'
        }]
      }, {
        test: /\.styl$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'postcss-loader'
        }, {
          loader: 'stylus-loader'
        }]
      }, {
        test: /.*\.(gif|png|ico|jpe?g|svg)$/i,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]',
            publicPath: 'images/',
            outputPath: 'images/'
          }
        }]
      }]
  }
};
