# webeditor
Blockly web editor to create and share turtle movements


## Build and run webeditor locally

    yarn install
    yarn run local

## Build docker image

To build docker image, after cloning repository, you should do the following:

```
    docker build -t webeditor .
```

## Run docker image

### Developer

To run the generated image, and share your local path for development purposes:

```
    docker run -it --rm --name webeditor -p 9000:9000 -v "$PWD":/src/ webeditor
```

The container will be terminated after use, if you want to use it in persistent mode:

```
    docker run -it -d --name webeditor -p 9000:9000 -v "$PWD":/src/ webeditor
```

Further invocations could be done only starting and stopping the container:

```
    docker stop webeditor
    docker start webeditor
```

### Test

If you are planning to use the container to test, the code was included in the image, so there is no need to pass the path to the container if you are not planning to develop:

```
    docker run -it -d --name webeditor -p 9000:9000 webeditor
```

In any case, **webeditor** will be available at `http://127.0.0.1:9000`. 


