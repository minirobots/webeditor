import React from 'react';
import { Provider, connect } from 'react-redux';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Home from './containers/Home/Home.jsx';
import Register from './containers/Register/Register.jsx';
import SignIn from './containers/SignIn/SignIn.jsx';
import AppContainer from './components/AppContainer.jsx';
import NoMatch from './containers/NoMatch/NoMatch.jsx';

import './App.styl';

class App extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    token: PropTypes.string
  };

  render() {
    const { store, token } = this.props;

    return (
      <Provider store={store}>
        <Router>
          {token === null ?
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/auth/:code?" component={SignIn} />
              <Route path="/register" component={Register} />
              <Route component={NoMatch} />
            </Switch> :
            <Switch>
              <Route exact path="/" component={AppContainer} />
              <Route path="/editor" component={AppContainer} />
              <Route path="/projects" component={AppContainer} />
              <Route path="/community" component={AppContainer} />
              <Redirect from='/register' to='/' />
              <Redirect from='/auth' to='/' />
              <Route path="*" component={NoMatch} />
            </Switch>
          }
        </Router>
      </Provider>
    );
  }
}

const mapStateToProps = state => {
  const { token } = state.app;

  return ({
    name: 'Juani',
    token
  });
};

export default connect(mapStateToProps)(App);
