import React from 'react';
import SocialLogin from 'react-social-login';
import PropTypes from 'prop-types';

// const Button = ({ children, triggerLogin, ...props }) => (
//   <button onClick={triggerLogin} {...props}>
//     { children }
//   </button>
// );

class Button extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
    ]),
    triggerLogin: PropTypes.func.isRequired,
    triggerLogout: PropTypes.func
  }

  render() {
    const {
      children, triggerLogin, triggerLogout, ...props
    } = this.props;

    const style = {
      background: '#eee',
      border: '1px solid black',
      borderRadius: '3px',
      display: 'inline-block',
      margin: '5px',
      padding: '10px 20px'
    };

    return (
      <div onClick={triggerLogin} style={style} {...props}>
        { children }
      </div>
    );
  }
}

export default SocialLogin(Button);
