import React from 'react';
import ReactDOM from 'react-dom';

import App from './App.jsx';
import configureStore from './store';

import './assets/images/favicon-16x16.png';
import './assets/images/favicon-32x32.png';
import './assets/images/favicon.ico';

const store = configureStore();

ReactDOM.render(<App store={store}/>, document.getElementById('app'));
