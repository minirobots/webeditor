import React from 'react';

import { Link } from 'react-router-dom';

import './Navbar.styl';

class Navbar extends React.Component {
  render() {
    return (
      <section className="navbar-component">
        <Link className="navbar-link" to="/editor">Editor</Link>
        <Link className="navbar-link" to="/projects">Proyectos</Link>
        <Link className="navbar-link" to="/community">Comunidad</Link>
      </section>
    );
  }
}

export default Navbar;
