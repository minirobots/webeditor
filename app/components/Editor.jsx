import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Blockly from 'node-blockly/browser';
import Interpreter from 'js-interpreter';
import lodash from 'lodash';
import { Button } from 'antd';
import classNames from 'classnames';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

import { getUserData, exportToRobot } from '../containers/Editor/actions';
import blocklyBlocksConfig from '../blocklyBlocksConfig.xml';
import './Editor.styl';

let aux = 0;

function initApi(interpreter, scope) {
  // Add an API function for the alert() block, generated for "text_print" blocks.
  // interpreter.setProperty(scope, 'pencil',
  //   interpreter.createNativeFunction(value => {
  //     console.log('PENCIL => ' + value);
  //     return value;
  //   })
  // );

  interpreter.setProperty(
    scope,
    'console',
    interpreter.createNativeFunction(text => console.log('results', text))
  );
}

class Editor extends React.Component {
  state = {
    ctx: null,
    canvas: null,

    blocklyContainer: null,
    workspace: null,

    blocklyData: null,

    speed: 1
  }

  static request(requestData) {
    const http = new XMLHttpRequest();
    const serverIp = document.getElementById('server-ip').value || null;
    const url = serverIp ? `http://${serverIp}/json` : 'http://192.168.0.18/json';
    http.open('POST', url, true);

    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = () => {
      if (http.readyState === 4 && http.status === 200) {
        console.log('SUCCESS!');
        console.log(http.responseText);
      }
    };

    http.send(JSON.stringify(requestData));
  }

  exportCode() {
    const { workspace } = this.state;
    const blocklyCode = Blockly.JavaScript.workspaceToCode(workspace);
    const test = `var params = [];
    var waypoints = [{ x: 150, y: 150 }];
    var x = 150;
    var y = 150;
    var angleInRadians = 0;
    var angleInDegrees = 0;
    var pencil = false;

    function forward(value) {
      x += value * Math.cos(angleInDegrees * (Math.PI / 180)).toFixed(2);
      y += value * Math.sin(angleInDegrees * (Math.PI / 180)).toFixed(2);

      waypoints.push({ x: x, y: y });
      params.push(["forward", value]);
    }

    function backward(value) {
      x -= value * Math.cos(angleInRadians);
      y -= value * Math.sin(angleInRadians);

      waypoints.push({ x: x, y: y });
      params.push(["backward", value]);
    }

    function left(degrees) {
      angleInRadians -= degrees * (Math.PI / 180);
      angleInDegrees -= degrees;

      params.push(["left", degrees]);
    }

    function right(degrees) {
      angleInRadians += degrees * (Math.PI / 180);
      angleInDegrees += degrees;

      params.push(["right", degrees]);
    }

    function pen(value) {
      params.push(["pen", value])
    }

    function end() {
      return JSON.stringify({params: params, waypoints: waypoints});
    }
    `;

    const result = 'end();';

    const code = `${test} ${blocklyCode} ${result}`;

    try {
      const myInterpreter = new Interpreter(code, initApi);
      // this.nextStep(myInterpreter);

      // while (myInterpreter.step()) {
      //   if (myInterpreter.value && myInterpreter.value.data && JSON.parse(myInterpreter.value.data)) {
      //     console.log(JSON.parse(myInterpreter.value.data))
      //     this.setState({
      //       blocklyData: JSON.parse(myInterpreter.value.data)
      //     });
      //   }
      //   myInterpreter.step();
      // }

      myInterpreter.run();

      if (myInterpreter.value && myInterpreter.value.data && JSON.parse(myInterpreter.value.data)) {
        this.setState({
          blocklyData: JSON.parse(myInterpreter.value.data)
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  // getVertices() {
  //   const { workspace } = this.state;
  //   const code = Blockly.JavaScript.workspaceToCode(workspace);
  //
  //   // reset values
  //   // params = [];
  //   // x = 150;
  //   // y = 150;
  //   // waypoints = [{ x, y }];
  //   // angleInRadians = 0;
  //   // angleInDegrees = 0;
  //
  //   try {
  //     eval(code);
  //   } catch (e) {
  //     console.log(e);
  //   }
  //
  //   return waypoints;
  // }

  getWaypoints(start, end) {
    const waypoints = [];
    const { speed } = this.state;
    const dx = end.x - start.x;
    const dy = end.y - start.y;

    let longest = Math.abs(dx) > Math.abs(dy) ? Math.abs(dx) : Math.abs(dy);
    // longest = (longest / speed) < 1 ? 1 : (longest / speed);
    // console.log(longest);
    for (let j = 0; j <= longest; j += 1) {
      const x = start.x + ((dx * j) / longest);
      const y = start.y + ((dy * j) / longest);
      console.log(`${x},${y}`);
      waypoints.push({ x, y });
    }

    return waypoints;
  }

  constructor(props) {
    super(props);

    this.blocklyEditorRef = React.createRef();
    this.canvasRef = React.createRef();

    this.x = 0;
    this.y = 0;
    this.params = [];
    this.waypoints = [];
    this.angleInRadians = 0;
    this.angleInDegrees = 0;
    this.pencil = false;

    // Color override
    Blockly.Msg.TURTLE = '#2196F3';
    Blockly.Msg.LOGIC_HUE = '#E91E63';
    Blockly.Msg.LOOPS_HUE = '#9C27B0';
    Blockly.Msg.MATH_HUE = '#3F51B5';
    Blockly.Msg.VARIABLES_HUE = '#FFC107';

    Blockly.Msg.LISTS_HUE = '#607D8B';
    Blockly.Msg.TEXTS_HUE = '#607D8B';
    Blockly.Msg.PROCEDURES_HUE = '#607D8B';
    Blockly.Msg.COLOUR_HUE = '#607D8B';

    // Block definitions
    Blockly.Blocks.turtle_pen = {
      init: function turtlePen() {
        this.appendDummyInput()
          .appendField('lapiz')
          .appendField(new Blockly.FieldDropdown([['subir', 'up'], ['bajar', 'down']]), 'pen');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite subir o bajar el lapiz');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/pen');
      }
    };

    Blockly.Blocks.turtle_turn = {
      init: function turtleTurn() {
        this.appendValueInput('turn')
          .setCheck('Number')
          .appendField(new Blockly.FieldDropdown([['girar a la izquierda ↺ ', 'left'], ['girar a la derecha ↻', 'right']]), 'turn');
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite girar hacia la derecha o hacia la izquierda una cantidad de grados definida');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/turn');
      }
    };

    Blockly.Blocks.turtle_move = {
      init: function turtleMove() {
        this.appendValueInput('move')
          .setCheck('Number')
          .appendField(new Blockly.FieldDropdown([['moverse adelante', 'forward'], ['moverse atras', 'backward']]), 'direction');
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite moverse hacia adelante o hacia atras la cantidad de pasos definida');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/move');
      }
    };

    // Code Generators
    Blockly.JavaScript.turtle_pen = block => {
      const dropdownPen = block.getFieldValue('pen') === 'up' ? 1 : 0;
      const code = `pen(${dropdownPen});\n`;
      return code;
    };
    Blockly.JavaScript.turtle_turn = block => {
      const dropdownTurn = block.getFieldValue('turn');
      const valueTurn = Blockly.JavaScript.valueToCode(block, 'turn', Blockly.JavaScript.ORDER_ATOMIC);
      const code = `this.${dropdownTurn}(${valueTurn});\n`;
      return code;
    };
    Blockly.JavaScript.turtle_move = block => {
      const dropdownDirection = block.getFieldValue('direction');
      const valueMove = Blockly.JavaScript.valueToCode(block, 'move', Blockly.JavaScript.ORDER_ATOMIC);
      const code = `this.${dropdownDirection}(${valueMove});\n`;
      return code;
    };
  }

  handleExport = async () => {
    const { user } = this.props;
    this.props.dispatch(getUserData({ token: this.props.token }));
    await this.exportCode();
    const code = this.state.blocklyData;
    if (code) {
      this.props.dispatch(exportToRobot({ instructions: code.params, ip: user.robots[0].last_event.ip }));
    }
  }

  handleDraw = async () => {
    const ctx = this.state.canvas.getContext('2d');
    ctx.globalCompositeOperation = 'destination-over';
    ctx.strokeStyle = '#424242';
    ctx.lineWidth = 2;

    await this.exportCode();

    ctx.clearRect(0, 0, 300, 300);
    const vertices = this.state.blocklyData.waypoints;
    const totalWaypoints = [];

    aux += 1;

    console.log('VERTICES ===>');
    console.log(vertices);
    console.log('VERTICES ===>');

    for (let i = 1; i < vertices.length; i += 1) {
      totalWaypoints.push(this.getWaypoints(vertices[i - 1], vertices[i]));
    }

    this.draw(lodash.flattenDeep(totalWaypoints), 1, aux);
  }

  draw(waypoints, position = 1, drawNumber) {
    if (drawNumber === aux) {
      if (position < waypoints.length - 1) {
        window.requestAnimationFrame(() => this.draw(waypoints, position + 1, drawNumber));
      }

      const { ctx } = this.state;

      ctx.beginPath();
      ctx.moveTo(waypoints[position - 1].x, waypoints[position - 1].y);
      ctx.lineTo(waypoints[position].x, waypoints[position].y);
      ctx.stroke();
    }
  }

  componentDidMount() {
    // startup blockly injection
    const blocklyContainer = document.getElementById('blockly-container');
    const blocklyEditor = this.blocklyEditorRef.current;
    const canvas = this.canvasRef.current;
    const workspace = Blockly.inject(blocklyEditor, {
      media: './blockly/media/',
      toolbox: blocklyBlocksConfig,
      grid: {
        spacing: 25,
        length: 3,
        colour: '#ccc',
        snap: true
      },
      zoom: {
        controls: true,
        wheel: false,
        startScale: 1.0,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2
      },
      trashcan: true
    });

    const xml = `
      <xml>
        <block type="controls_repeat_ext">
          <value name="TIMES">
            <shadow type="math_number">
              <field name="NUM">5</field>
            </shadow>
          </value>
          <statement name="DO">
            <block type="turtle_move">
              <field name="direction">forward</field>
              <value name="move">
                <shadow type="math_number">
                  <field name="NUM">50</field>
                </shadow>
              </value>

              <next>
                <block type="turtle_turn">
                  <field name="turn">right</field>
                  <value name="turn">
                    <shadow type="math_number">
                      <field name="NUM">144</field>
                    </shadow>
                  </value>
                </block>
              </next>
            </block>
          </statement>
        </block>
      </xml>`;

    const juani = Blockly.Xml.textToDom(xml);
    Blockly.Xml.domToWorkspace(juani, workspace);

    const ctx = canvas.getContext('2d');
    ctx.globalCompositeOperation = 'destination-over';
    ctx.strokeStyle = 'red';
    ctx.lineWidth = 2;

    this.setState({
      ctx,
      canvas,
      blocklyContainer,
      workspace
    });
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  render() {
    const { user } = this.props;

    return (
      <section className={classNames('editor-component', this.props.className)}>
        <section id="blockly-container" className="blockly-container">
          <div id="blockly-editor" ref={this.blocklyEditorRef}></div>
        </section>

        <section className="right-panel">
          <canvas
            className="canvas"
            id="canvas"
            width="300"
            height="300"
            ref={this.canvasRef}></canvas>
          <section className="actions">
            <Button
              type="primary"
              onClick={this.handleExport}
              disabled={!user}>
              Exportar
            </Button>
            <Button
              type="primary"
              onClick={this.handleDraw}>
              Dibujar
            </Button>
            {/* <InputRange
              maxValue={10}
              minValue={1}
              className="input-range"
              value={this.state.speed}
              onChange={speed => this.setState({ speed })} /> */}

            {/* {JSON.stringify(this.state.blocklyData)} */}

            {user ?
              <span className="font-sm">IP: {user.robots[0].last_event.ip}</span> :
              <span className="font-sm">ERROR: No User</span>}
          </section>
        </section>
      </section>
    );
  }
}

Editor.propTypes = {
  className: PropTypes.string
};

export default connect()(Editor);
