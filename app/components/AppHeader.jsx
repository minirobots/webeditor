import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';

import logo from '../assets/images/logo.jpg';
import './AppHeader.styl';

class AppHeader extends React.Component {
  handleLogout = () => {
    localStorage.clear();
    this.props.history.push('/');
  }

  render() {
    const { user } = this.props;

    return (
      <header className="app-header-component paper-shadow-1 position-relative">
        <section className="app-header-container">
          <Link to="/">
            <img src={logo} className="image-logo"/>
          </Link>
          <div className="flex-grow-1"></div>
          <span className="userEmail">{user && user.email}</span>
          <Icon className="icon" type="logout" title="Salir" onClick={this.handleLogout} />
        </section>
      </header>
    );
  }
}

const mapStateToProps = state => {
  const { user } = state.app;

  return ({
    user
  });
};

export default connect(mapStateToProps)(AppHeader);

