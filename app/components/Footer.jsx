import React from 'react';

import './Footer.styl';

const Footer = () => (
  <footer className="footer-component">
    <section className="container center">
      <span>© {new Date().getFullYear()} - Minirobots.</span>
    </section>
  </footer>
);

export default Footer;
