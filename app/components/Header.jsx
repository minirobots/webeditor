import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

import logo from '../assets/images/logo.jpg';
import './Header.styl';

class Header extends React.Component {
  static propTypes = {
    loginButton: PropTypes.bool
  };

  static defaultProps = {
    loginButton: true
  }

  render() {
    return (
      <header className="header-component paper-shadow-1">
        <section>
          <section className="header-container center">
            <Link to="/">
              <img src={logo} className="image-logo" />
            </Link>

            {this.props.loginButton &&
            <section className="btn-container">
              <Link className="btn" to="/auth">
                <Button type="primary">Ingresar</Button>
              </Link>
            </section>
            }
          </section>
        </section>
      </header>
    );
  }
}

export default Header;
