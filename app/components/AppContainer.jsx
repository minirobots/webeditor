import React from 'react';
import { Route } from 'react-router-dom';

import AppHeader from '../components/AppHeader.jsx';
import Navbar from '../components/Navbar.jsx';
import Editor from '../containers/Editor/Editor.jsx';
import Projects from '../containers/Projects/Projects.jsx';
import Community from '../containers/Community/Community.jsx';

import './AppContainer.styl';

class AppContainer extends React.Component {
  render() {
    return (
      <section className="app-container-component">
        <AppHeader />
        <section className="app-container-component-content">
          <Navbar />
          <Route exact path="/editor" component={Editor} />
          <Route exact path="/projects" component={Projects} />
          <Route exact path="/community" component={Community} />
          <Route exact path="/" component={Editor} />
        </section>
      </section>
    );
  }
}

export default AppContainer;
