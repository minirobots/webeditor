export const getUserData = payload => ({
  type: 'API_GET_USER_DATA',
  token: payload.token
});

export const exportToRobot = payload => ({
  type: 'ROBOT_SEND_CODE',
  instructions: payload.instructions,
  ip: payload.ip
});

