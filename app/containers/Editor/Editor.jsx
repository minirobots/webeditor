import React from 'react';
import { connect } from 'react-redux';
import EditorComponent from '../../components/Editor.jsx';
import './Editor.styl';

class Editor extends React.Component {
  render() {
    const { token, user } = this.props;

    return (
      <div className="editor-container w-100">
        <EditorComponent token={token} user={user} className="m-10" />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { token, user } = state.app;

  return ({
    token,
    user
  });
};

export default connect(mapStateToProps)(Editor);

