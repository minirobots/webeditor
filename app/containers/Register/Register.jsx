import React from 'react';
import { Link } from 'react-router-dom';

import DocumentTitle from 'react-document-title';

import './Register.styl';

class Register extends React.Component {
  render() {
    return (
      <DocumentTitle title='Tempos - Registrarse'>
        <div className="register-container">
          <h1>Register Page</h1>
          <Link to="/sign-in">Ingresar</Link>
          <br />
          <Link to="/">Home</Link>
        </div>
      </DocumentTitle>
    );
  }
}

export default Register;
