import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';

import './NoMatch.styl';

class NoMatch extends React.Component {
  render() {
    return (
      <div className="nomatch-component">
        <h1 className="title">404</h1>
        <h3>No podemos encontrar lo que estas buscando</h3>
        <Link className="link" to="/">Volver al inicio</Link>
        <img src={logo} className="image-logo" />
      </div>
    );
  }
}

export default NoMatch;
