import React from 'react';

import Header from '../../components/Header.jsx';
import Footer from '../../components/Footer.jsx';

import './Home.styl';

class Home extends React.Component {
  render() {
    return (
      <section className="home-container">
        <Header />
        <section className="home-content">
          <h1>Minirobots</h1>
        </section>
        <Footer />
      </section>
    );
  }
}

export default Home;
