export const signIn = payload => ({
  type: 'API_SIGNIN_REQUEST',
  payload
});

export const signInGoogle = payload => ({
  type: 'GOOGLE_SIGNIN_REQUEST',
  payload
});
