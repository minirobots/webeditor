import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import DocumentTitle from 'react-document-title';
import { GoogleLogin } from 'react-google-login';
import queryString from 'query-string';

import Header from '../../components/Header.jsx';
import Footer from '../../components/Footer.jsx';

import SocialButton from '../../toolkit/SocialButton.jsx';

import { signIn, signInGoogle } from './actions';

import './SignIn.styl';

class SignIn extends React.Component {
  state = {
    email: '',
    pass: ''
  }

  handleSignIn = () => {
    this.props.dispatch(signIn())
  };

  // handleGoogleSignIn = () => {
  //   this.props.dispatch(signInGoogle());
  // }

  handleSocialLogin = () => {
  }

  handleSocialLoginFailure = () => {
  }

  render() {
    const redirectURIs = {
        'local'       : 'http://localhost:9000/auth',
        'development' : 'https://editor.dev.minirobots.com.ar/auth',
        'production'  : 'https://editor.minirobots.com.ar/auth',
    };
    const googleLink = 'https://accounts.google.com/o/oauth2/auth?redirect_uri=' + redirectURIs[process.env.NODE_ENV] + '&client_id=24480484268-svrdr9iqpj9h2vhmtvrjbeiet5ghiusn.apps.googleusercontent.com&response_type=code&approval_prompt=auto&scope=profile+email';

    const query = queryString.parse(this.props.location.search);
    if (query.code) {
      this.props.dispatch(signIn(query.code));
    }

    return (
      <DocumentTitle title='Minirobots - Ingresar'>
        <section>
          <Header />
          <div className="signin-container">
            <div className="spring"></div>
            <h1>Ingresar</h1>
            <section className="form-container card">
              {/* <TextField floatingLabelText="email" />
              <TextField floatingLabelText="contraseña" />
              <RaisedButton
                label="Ingresar"
                primary={true}
                onClick={this.handleSignIn} />
              <div onClick={this.handleGoogleSignIn}>Google</div> */}

              {/* <SocialButton
                provider='google'
                appId='24480484268-svrdr9iqpj9h2vhmtvrjbeiet5ghiusn.apps.googleusercontent.com'
                onLoginSuccess={this.handleSocialLogin}
                onLoginFailure={this.handleSocialLoginFailure}
              >
                Login with Google
              </SocialButton> */}

              <a href={googleLink} className="googleLogin">
                <svg
                  viewBox="0 0 18 18"
                  role="presentation"
                  aria-hidden="true"
                  focusable="false"
                  style={{ height: '18px', width: '18px', display: 'block' }}>
                  <g fill="none">
                    <path d="M9 3.48c1.69 0 2.83.73 3.48 1.34l2.54-2.48C13.46.89 11.43 0 9 0 5.48 0 2.44 2.02.96 4.96l2.91 2.26C4.6 5.05 6.62 3.48 9 3.48z" fill="#EA4335"></path>
                    <path d="M17.64 9.2c0-.74-.06-1.28-.19-1.84H9v3.34h4.96c-.1.83-.64 2.08-1.84 2.92l2.84 2.2c1.7-1.57 2.68-3.88 2.68-6.62z" fill="#4285F4"></path>
                    <path d="M3.88 10.78A5.54 5.54 0 0 1 3.58 9c0-.62.11-1.22.29-1.78L.96 4.96A9.008 9.008 0 0 0 0 9c0 1.45.35 2.82.96 4.04l2.92-2.26z" fill="#FBBC05"></path>
                    <path d="M9 18c2.43 0 4.47-.8 5.96-2.18l-2.84-2.2c-.76.53-1.78.9-3.12.9-2.38 0-4.4-1.57-5.12-3.74L.97 13.04C2.45 15.98 5.48 18 9 18z" fill="#34A853"></path>
                    <path d="M0 0h18v18H0V0z"></path>
                  </g>
                </svg>
                <span>Iniciar sesión con Google</span>
              </a>

              {/* <div className="g-signin2" data-onsuccess="onSignIn"></div>

              <GoogleLogin
                clientId="24480484268-svrdr9iqpj9h2vhmtvrjbeiet5ghiusn.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
              /> */}

              <br />
              <div className="links">
                <span><Link to="/">Ir al inicio</Link></span>
              </div>
            </section>
          </div>
          <Footer />
        </section>
      </DocumentTitle>
    );
  }
}

export default connect()(SignIn);
