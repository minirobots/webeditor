import { delay } from 'redux-saga';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import axios from 'axios';

let apiURL = '';
let redirectURL = '';

if (process.env.NODE_ENV === 'local') {
  apiURL = 'http://localhost:5000';
  redirectURL = 'http://localhost:9000/auth';
}

if (process.env.NODE_ENV === 'development') {
  apiURL = 'https://api.dev.minirobots.com.ar';
  redirectURL = 'https://editor.dev.minirobots.com.ar/auth';
}

if (process.env.NODE_ENV === 'production') {
  apiURL = 'https://api.minirobots.com.ar';
  redirectURL = 'https://editor.minirobots.com.ar/auth';
}

export function* logSaga() {
  yield takeLatest('*', action => {
    console.log(action);
  });
}

export function* incrementAsync() {
  yield call(delay, 1000);
  yield put({ type: 'INCREMENT' });
}

export function* watchIncrementAsync() {
  yield takeLatest('INCREMENT_ASYNC', incrementAsync);
}

export function* signin(payload) {
  try {
    const res = yield axios.get(`${apiURL}/auth/google`, {
      params: {
        code: payload.payload,
        redirect_uri: redirectURL
      }
    });

    yield put({ type: 'API_SIGNIN_SUCCESS', response: { token: res.data.token } });
    yield put({ type: 'API_GET_USER_DATA', token: res.data.token });
  } catch (err) {
    console.log(err);
  }

  // OPCION NUMERO UNO - FALLA NO ENTRA AL THEN
  // axios.get('https://api.dev.minirobots.com.ar/auth/google', {
  //   params: {
  //     code: payload.payload,
  //     redirect_uri: 'http://localhost:9000/auth'
  //   }
  // })
  //   .then(function* response(res) {
  //     console.log('fin del login!!!');
  //     console.log(res);
  //     console.log('fin del login!!!');
  //     yield put({ type: 'API_SIGNIN_SUCCESS', response: { token: res.data.token } });
  //   })
  //   .catch(err => {
  //     console.log(err);
  //   });
}

export function* getUserData({ token }) {
  yield put({ type: 'API_GET_USER_DATA_REQUESTED' });
  try {
    const res = yield axios.get(`${apiURL}/users`, {
      params: {
        access_token: token
      }
    });
    yield put({ type: 'API_GET_USER_DATA_SUCCESS', user: res.data.user });
  } catch (err) {
    console.log(err);
  }
}

export function* googleSignin() {
  // axios.get('https://api.minirobots.com.ar/auth/google', {
  //   crossdomain: true,
  //   withCredentials: true,
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //     'Content-Type': 'application/json'
  //   }
  // })
  //   .then(res => {
  //     console.log('fin del login');
  //     console.log(res);
  //     console.log('fin del login');
  //   });
  // yield put({ type: 'API_SIGNIN_SUCCESS', response: { token: '1234' } });
}

export function* robotSendCode({ instructions, ip }) {
  yield put({ type: 'ROBOT_SEND_CODE_REQUESTED' });
  console.log("commands", instructions);
  try {
    yield axios.post('', {'commands': instructions}, {baseURL: `http://${ip}/json`});
    yield put({ type: 'ROBOT_SEND_CODE_SUCCESS' });
  } catch (err) {
    console.log(err);
  }
}

// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield [
    logSaga(),
    watchIncrementAsync(),

    takeLatest('API_SIGNIN_REQUEST', signin),
    takeLatest('GOOGLE_SIGNIN_REQUEST', googleSignin),
    takeLatest('API_GET_USER_DATA', getUserData),
    takeLatest('ROBOT_SEND_CODE', robotSendCode)
  ];
}
