import { extend, get } from 'lodash/fp';
import { placeholderAuth, placeholderUser } from 'browser-utils';

export function initialState() {
  return {
    number: 0,
    max: 50,
    token: placeholderAuth(),
    user: placeholderUser()
  };
}

export const handlers = {
  INCREMENT: (state, { value }) => ({
    number: state.number + value
  }),

  SET_MAX: (state, { max }) => ({
    max
  }),

  API_SIGNIN_SUCCESS: (state, { response }) => {
    const token = get('token', response);

    if (token) {
      localStorage.setItem('token', token);
      placeholderAuth(token);
    }

    return { token };
  },

  API_GET_USER_DATA_SUCCESS: (state, { user }) => {
    if (user && user !== state.user) {
      localStorage.setItem('user', JSON.stringify(user));
      placeholderUser(user);
    }
  }
};

export default function app(state = initialState(), action) {
  const fn = handlers[action.type];
  return fn ? extend(state, fn(state, action)) : state;
}
