import React from 'react';
import ReactDOM from 'react-dom';

import lodash from 'lodash';
import './base.styl';

//
// import App from './components/App.jsx';
// import configureStore from './store';
//
// const store = configureStore();
//
// ReactDOM.render(<App store={store}/>, document.getElementById('app'));


// Color override
Blockly.Msg.TURTLE = '#2196F3';
Blockly.Msg.LOGIC_HUE = '#E91E63';
Blockly.Msg.LOOPS_HUE = '#9C27B0';
Blockly.Msg.MATH_HUE = '#3F51B5';
Blockly.Msg.VARIABLES_HUE = '#FFC107';

Blockly.Msg.LISTS_HUE = '#607D8B';
Blockly.Msg.TEXTS_HUE = '#607D8B';
Blockly.Msg.PROCEDURES_HUE = '#607D8B';
Blockly.Msg.COLOUR_HUE = '#607D8B';

// Block definitions
Blockly.Blocks['turtle_pen'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("lapiz")
            .appendField(new Blockly.FieldDropdown([["subir","up"], ["bajar","down"]]), "pen");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite subir o bajar el lapiz');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/pen');
    }
};

Blockly.Blocks['turtle_turn'] = {
    init: function() {
        this.appendValueInput("turn")
            .setCheck("Number")
            .appendField(new Blockly.FieldDropdown([["girar a la izquierda ↺ ","left"], ["girar a la derecha ↻","right"]]), "turn");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite girar hacia la derecha o hacia la izquierda una cantidad de grados definida');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/turn');
    }
};

Blockly.Blocks['turtle_move'] = {
    init: function() {
        this.appendValueInput("move")
            .setCheck("Number")
            .appendField(new Blockly.FieldDropdown([["moverse adelante","forward"], ["moverse atras","backward"]]), "direction");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(Blockly.Msg.TURTLE);
        this.setTooltip('permite moverse hacia adelante o hacia atras la cantidad de pasos definida');
        this.setHelpUrl('http://docs.minirobots.com.ar/turtle/move');
    }
};

// Code Generators
Blockly.JavaScript['turtle_pen'] = (block) => {
    var dropdown_pen = block.getFieldValue('pen') === 'up' ? 1 : 0;
    var code = `pencil(${dropdown_pen});\n`;
    return code;
};
Blockly.JavaScript['turtle_turn'] = (block) => {
    var dropdown_turn = block.getFieldValue('turn');
    var value_turn = Blockly.JavaScript.valueToCode(block, 'turn', Blockly.JavaScript.ORDER_ATOMIC);
    var code = `${dropdown_turn}(${value_turn});\n`;
    return code;
};
Blockly.JavaScript['turtle_move'] = (block) => {
    var dropdown_direction = block.getFieldValue('direction');
    var value_move = Blockly.JavaScript.valueToCode(block, 'move', Blockly.JavaScript.ORDER_ATOMIC);
    var code = `${dropdown_direction}(${value_move});\n`;
    return code;
};

// startup blockly injection
const blocklyContainer = document.getElementById('blockly-container');
const blocklyEditor = document.getElementById('blockly-editor');
const workspace = Blockly.inject(blocklyEditor, {
    media: './blockly/media/',
    toolbox: document.getElementById('toolbox'),
    grid: {
        spacing: 25,
        length: 3,
        colour: '#ccc',
        snap: true
    },
    zoom: {
        controls: true,
        wheel: false,
        startScale: 1.0,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2
    },
    trashcan: true
});


let params = [];
let waypoints = [];
let x = 0;
let y = 0;
let angleInRadians = 0;
let angleInDegrees = 0;
let pencil = false;

function forward(value) {
    x += value * Math.cos(angleInDegrees * (Math.PI / 180)).toFixed(2);
    y += value * Math.sin(angleInDegrees * (Math.PI / 180)).toFixed(2);

    console.log(`x=>${x} y=>${y}`);

    waypoints.push({ x, y });
    params.push({
        c: 'forward',
        a: value
    })
}
function backward(value) {
    x -= value * Math.cos(angleInRadians);
    y -= value * Math.sin(angleInRadians);

    waypoints.push({ x, y });

    params.push({
        c: 'backward',
        a: value
    })
}
function left(degrees) {
    angleInRadians -= degrees * (Math.PI / 180);
    angleInDegrees -= degrees;

    params.push({
        c: 'left',
        a: degrees
    })
}
function right(degrees) {
    angleInRadians += degrees * (Math.PI / 180);
    angleInDegrees += degrees;

    params.push({
        c: 'right',
        a: degrees
    })
}
function pencil(value) {
    params.push({
        pencil: value
    })
}

function request(requestData) {
    const http = new XMLHttpRequest();
    const serverIp = document.getElementById('server-ip').value || null;
    const url = serverIp ? `http://${serverIp}/json` : 'http://192.168.0.18/json';
    http.open('POST', url, true);

    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = () => {
        if (http.readyState === 4 && http.status === 200) {
            console.log('SUCCESS!');
            console.log(http.responseText);
        }
    };

    http.send(JSON.stringify(requestData));
}

function exportCode() {
    const code = Blockly.JavaScript.workspaceToCode(workspace);
    params = [];
    x = 0;
    y = 0;
    waypoints = [{ x, y }];
    angleInRadians = 0;
    angleInDegrees = 0;

    try {
        eval(code);
    } catch (e) {
        console.log(e);
    }

    console.log(`CODE:\n${code}\n`);
    console.log(`PARAMS:\n${JSON.stringify(params)}\n`);
    console.log(`WAYPOINTS:\n${JSON.stringify(waypoints)}\n`);

    request({
        cmds: params
    });
}

function getVertices() {
    const code = Blockly.JavaScript.workspaceToCode(workspace);
    params = [];
    x = 150;
    y = 150;
    waypoints = [{ x, y }];
    angleInRadians = 0;
    angleInDegrees = 0;

    try {
        eval(code);
    } catch (e) {
        console.log(e);
    }

    return waypoints;
}


var ctx = document.getElementById('canvas').getContext('2d');
ctx.globalCompositeOperation = 'destination-over';
ctx.strokeStyle = "red";
ctx.lineWidth = 2;

function getWaypoints(start, end) {
    let waypoints = [];
    const speed = document.getElementById('speed').value;
    let dx = end.x - start.x;
    let dy = end.y - start.y;

    let longest = Math.abs(dx) > Math.abs(dy) ? Math.abs(dx) : Math.abs(dy);
    longest = (longest / speed) < 1 ? 1 : (longest / speed);
    //console.log(longest);
    for (var j = 0; j <= longest; j++) {
        var x = start.x + dx * j / longest;
        var y = start.y + dy * j / longest;
        waypoints.push({
            x: x,
            y: y
        });
    }

    return waypoints;
}

function draw(waypoints, position = 1) {
    if (position < waypoints.length - 1) {
        window.requestAnimationFrame(() => draw(waypoints, position + 1));
    }

    //console.log(`x=${waypoints[position].x};y=${waypoints[position].y}`);
    ctx.beginPath();
    ctx.moveTo(waypoints[position - 1].x, waypoints[position - 1].y);
    ctx.lineTo(waypoints[position].x, waypoints[position].y);
    ctx.stroke();
}

document.getElementById('export-code').onclick = () => exportCode();
document.getElementById('draw').onclick = () => {
    ctx.clearRect(0, 0, 300, 300);
    const vertices = getVertices();
    const totalWaypoints = [];
    for(var i=1; i<vertices.length; i++) {
        totalWaypoints.push(getWaypoints(vertices[i - 1], vertices[i]));
    }
    draw(lodash.flattenDeep(totalWaypoints))
};
