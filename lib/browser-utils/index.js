let accessToken = localStorage.getItem('token');
let userLocalStorage = JSON.parse(localStorage.getItem('user'));

function placeholderAuth(token = accessToken) {
  accessToken = token;
  return accessToken;
}

function placeholderUser(user = userLocalStorage) {
  userLocalStorage = user;
  return userLocalStorage;
}

function getStyle(oElm, cssRule) {
  if (document.defaultView && document.defaultView.getComputedStyle) {
    return document.defaultView.getComputedStyle(oElm, '').getPropertyValue(cssRule);
  } else if (oElm.currentStyle) {
    const windowsCssRule = cssRule.replace(/-(\w)/g, (match, p1) => p1.toUpperCase());
    return oElm.currentStyle[windowsCssRule];
  }
  return '';
}

export {
  placeholderAuth,
  getStyle,
  placeholderUser
};
